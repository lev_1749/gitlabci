from typing import TYPE_CHECKING

from sqlalchemy import Boolean, Column, Integer, String, Numeric
from sqlalchemy.orm import relationship

from app.db.base_class import Base

if TYPE_CHECKING:
    from .company import Company  # noqa: F401


class Scores(Base):
    id = Column(Integer, primary_key=True, index=True)
    year = Column(String, nullable=False)
    ebit = Column(Numeric(10,4), nullable=False)
    equity = Column(Numeric(10,4), nullable=False)
    retained_earnings = Column(Numeric(10,4), nullable=False)
    sales = Column(Numeric(10,4), nullable=False)
    total_assets = Column(Numeric(10,4), nullable=False)
    total_liabilities = Column(Numeric(10,4), nullable=False)
    working_capital = Column(Numeric(10,4), nullable=False)
    company = relationship("Company", back_populates="owner")



